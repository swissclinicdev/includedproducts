<?php
namespace Swiss\IncludedProducts\Catalog\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\App\ObjectManager;
use Swiss\IncludedProducts\Model\IncludedProductFactory;

class IncludedProducts extends AbstractModifier
{
    const CUSTOM_FIELDSET_INDEX    = 'included_products';
    const CUSTOM_FIELDSET_CHILDREN = 'included_product_';
    const CUSTOM_COMPONTENT_TYPE   = 'field';

    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface
     */
    private $_locator;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @param LocatorInterface $locator
     */
    public function __construct(
        LocatorInterface $locator
    ) {
        $this->_locator = $locator;
    }

    /**
     * Meta-data modifier: adds ours fieldset
     *
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        if (!isset($this->meta['gift-card-information'])) {
            $this->addCustomFieldset();
        }

        return $this->meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Merge existing meta-data with our meta-data (do not overwrite it!)
     *
     * @return void
     */
    protected function addCustomFieldset()
    {

        $this->meta = array_merge_recursive(
            $this->meta,
            [
                self::CUSTOM_FIELDSET_INDEX => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label'         => __('Included Products'),
                                'sortOrder'     => 50,
                                'collapsible'   => true,
                                'dataScope'     => static::DATA_SCOPE_PRODUCT, // save data in the product data
                                'provider'      => static::DATA_SCOPE_PRODUCT . '_data_source',
                                'ns'            => static::FORM_NAME,
                                'componentType' => 'fieldset',
                            ],
                        ],
                    ],
                    'children'  => $this->_getFormElements(),
                ],
            ]
        );
    }

    /**
     * @return array $list
     */
    private function _getProductsList()
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product           = $this->_locator->getProduct();
        $modelId           = $product->getId();
        $objectManager     = ObjectManager::getInstance();
        $productCollection = $objectManager->create(CollectionFactory::class);
        $collection        = $productCollection->create()
            ->addAttributeToSelect(
                '*'
            )->addFieldToFilter('type_id', ['neq' => 'mpgiftcard'])->load();

        $includedProducts   = $objectManager->create(IncludedProductFactory::class);
        $includedCollection = $includedProducts->create()->getCollection();
        $includedCollection->addFieldToFilter('product_id', $modelId);
        $collectionData = $includedCollection->getData();
        $list           = [];
        foreach ($collection as $product) {

            $data = [
                'value'   => $product->getId(),
                'label'   => $product->getName(),
                'checked' => false,
            ];

            foreach ($collectionData as $ic) {
                if ($ic['included_product_id'] == $product->getId()) {
                    $data['checked'] = true;
                    break 1;
                }
            }

            $list[] = $data;
        }
        return $list;
    }

    /**
     * @return array $items
     */
    private function _getFormElements()
    {
        $products     = $this->_getProductsList();
        $productCount = count($products);
        $items        = [];

        for ($i = 0; $i < $productCount; $i++) {
            $items[self::CUSTOM_FIELDSET_CHILDREN . $i] = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'formElement'   => 'checkbox',
                            'componentType' => self::CUSTOM_COMPONTENT_TYPE,
                            'value'         => $products[$i]['value'],
                            'visible'       => 1,
                            'required'      => 1,
                            'label'         => __($products[$i]['label']),
                            'checked'       => $products[$i]['checked'],
                        ],
                    ],
                ],
            ];
        }

        return $items;
    }
}
