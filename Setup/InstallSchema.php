<?php

namespace Swiss\IncludedProducts\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('swiss_included_product');
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            $table = $installer->getConnection()->newTable(
                $tableName
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                ],
                'ID'
            )->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => false,
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Product ID'
            )->addColumn(
                'included_product_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => false,
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Included product ID'
            )->setComment(
                'Defines relationship between a product and the products '
                . 'it includes.'
            );

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
