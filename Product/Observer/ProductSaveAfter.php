<?php
namespace Swiss\IncludedProducts\Product\Observer;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Ui\Component\MassAction\Filter;
use Swiss\IncludedProducts\Catalog\Ui\DataProvider\Product\Form\Modifier\IncludedProducts;
use Swiss\IncludedProducts\Model\IncludedProduct;
use Swiss\IncludedProducts\Model\IncludedProductFactory;
use Swiss\IncludedProducts\Model\ResourceModel\IncludedProduct as IPResource;

class ProductSaveAfter implements ObserverInterface
{
    /**
     * @var mixed
     */
    private $_includedProductFactory;

    /**
     * @var mixed
     */
    private $_filter;

    /**
     * @var mixed
     */
    private $_resourceModel;

    /**
     * @param Context $context
     * @param IncludedProductFactory $includedProductFactory
     */
    public function __construct(
        Context $context,
        IncludedProductFactory $includedProductFactory,
        IPResource $resourceModel,
        Filter $filter
    ) {
        $this->_includedProductFactory = $includedProductFactory;
        $this->_filter                 = $filter;
        $this->_resourceModel          = $resourceModel;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $om = ObjectManager::getInstance();
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();

        if (!$product) {
            return;
        }

        $includedProductsModel = $this->_includedProductFactory->create();
        $preexisting           = $includedProductsModel->getCollection(
        )->addFieldToFilter(
            'product_id', $product->getId()
        );

        foreach ($preexisting->getItems() as $preex) {
            $preex->delete();
        }

        $data             = $product->getData();
        $includedProducts = [];
        try {
            $includedProductsModel = $this->_includedProductFactory->create();
            foreach ($data as $key => $value) {
                if (strpos($key, IncludedProducts::CUSTOM_FIELDSET_CHILDREN) !== false) {
                    $model = $includedProductsModel;
                    $model->setData('product_id', $product->getId());
                    $model->setData('included_product_id', $value);
                    $model->save();
                }
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
        return $product;
    }
}
