<?php
namespace Swiss\IncludedProducts\Model\ResourceModel\IncludedProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
     * @var string
     */
    protected $_eventPrefix = 'swiss_included_product_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'included_product_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Swiss\IncludedProducts\Model\IncludedProduct',
            'Swiss\IncludedProducts\Model\ResourceModel\IncludedProduct'
        );
    }

}
