<?php

namespace Swiss\IncludedProducts\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class IncludedProduct extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'swiss_included_product';

    /**
     * @var string
     */
    protected $_cacheTag = 'swiss_included_product';
    /**
     * @var string
     */
    protected $_eventPrefix = 'swiss_included_product';

    protected function _construct()
    {
        $this->_init(
            'Swiss\IncludedProducts\Model\ResourceModel\IncludedProduct'
        );
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
